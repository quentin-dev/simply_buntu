FROM ubuntu:disco

RUN apt update
RUN apt install software-properties-common firefox python3 python3-pip wget -y

RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz
RUN tar -xvzf geckodriver*
RUN chmod +x geckodriver
RUN mv geckodriver /usr/local/bin
